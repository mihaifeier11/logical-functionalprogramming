% interclasare(L1: lista, L2: lista, Lr: lista)
% model de flux(i, i, o)
% L1 - Prima lista
% L2 - A 2-a lista
% Lr - Lista rezultat
interclasare([], [], []):-!.
interclasare([], [H2|T2], [H2|Lr]):-!,
    interclasare([], T2, Lr).
interclasare([H1|T1], [], [H1|Lr]):-!,
    interclasare(T1, [], Lr).
interclasare([H1|T1], [H2|T2], [H1|Lr]):-
    H1 < H2,!,
    interclasare(T1, [H2|T2], Lr).
interclasare([H1|T1], [H2|T2], [H2|Lr]):-
    H1 > H2,!,
    interclasare([H1|T1], T2, Lr).
interclasare([H1|T1], [H2|T2], [H2|Lr]):-
    H1 = H2,!,
    interclasare(T1, T2, Lr).


% interclasareLista(L: lista, Rez: lista)
% model de flux(i, o)
% L - lista din care trebuie extrase si interclasate sublistele
% Rez - lista rezultat
f([], []):-!.
f([H|T], Rez):-
    is_list(H), !,
    f(T, LR),
    interclasare(H, LR, Rez).
f([_|T], LR):-
    f(T, LR).




