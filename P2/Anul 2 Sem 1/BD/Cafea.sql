CREATE TABLE Baristas (
	bid INT,
	nume VARCHAR(20),
	varsta INT,
	experienta VARCHAR(20) NOT NULL,
	CHECK (experienta IN ('incepator', 'mediu', 'experimentat')),
	PRIMARY KEY(bid))

CREATE TABLE Shops (
	sid INT,
	nume VARCHAR(20),
	PRIMARY KEY(sid))

ALTER TABLE Baristas ADD sid INT
ALTER TABLE Baristas ADD FOREIGN KEY(sid) REFERENCES Shops(sid)

CREATE TABLE Managers (
	mid INT,
	nume VARCHAR(20),
	varsta INT,
	sid INT,
	PRIMARY KEY(mid),
	FOREIGN KEY(sid) REFERENCES Shops(sid))

ALTER TABLE Shops ADD mid INT
ALTER TABLE Shops ADD FOREIGN KEY(mid) REFERENCES Managers(mid)

CREATE TABLE TipuriBoabe (
	tbid INT,
	nume VARCHAR(20),
	gradPrajire INT NOT NULL,
	CHECK (gradPrajire BETWEEN 1 AND 5),
	taraProvenienta VARCHAR(20),
	PRIMARY KEY(tbid))

CREATE TABLE MixuriCafea (
	mixid INT,
	nume VARCHAR(20),
	PRIMARY KEY(mixid))

CREATE TABLE MixBoabe(
	mixid INT REFERENCES MixuriCafea(mixid),
	tbid INT REFERENCES TipuriBoabe(tbid),
	procentaj VARCHAR(20),
	PRIMARY KEY(mixid, tbid))

CREATE TABLE Retete(
	rid INT,
	nume VARCHAR(20),
	descriere VARCHAR(300),
	mixid INT,
	PRIMARY KEY(rid),
	FOREIGN KEY(mixid) REFERENCES MixuriCafea(mixid))

CREATE TABLE Ingrediente(
	iid INT,
	nume VARCHAR(20),
	tempDepozitare INT,
	PRIMARY KEY(iid))

CREATE TABLE ReteteIngrediente(
	rid INT REFERENCES Retete(rid),
	iid INT REFERENCES Ingrediente(iid),
	PRIMARY KEY(rid, iid))

CREATE TABLE BaristasRetete(
	rid INT REFERENCES Retete(rid),
	bid INT REFERENCES Baristas(bid),
	PRIMARY KEY(rid, bid))