INSERT INTO Procesoare(pid, nume, nucleu, frecventa) VALUES (1, 'Intel Core i5-7200U', 'Kaby Lake', 2.5)

INSERT INTO Procesoare(pid, nume, nucleu, frecventa) VALUES 
(2, 'Intel Core i5-8250U', 'Kaby Lake R', 1.6),
(3, 'Intel Core i5-5350U', 'Broadwell', 1.8),
(4, 'Intel Core i5-7100U', 'Kaby Lake', 2.4),
(5, 'Intel Core i7-8750U', 'Coffee Lake', 2.2),
(6, 'Intel Core i5-8250U', 'Kaby Lake R', 1.6),
(7, 'Intel Core i3-6006U', 'Skylake', 2.5),
(8, 'Intel Celeron N3350', 'Apollo Lake', 1.1),
(9, 'Intel Core i7-7700HQ', 'Kaby Lake', 2.8)

INSERT INTO HDD(hid, viteza, interfata, capacitate, tip) VALUES
(1, '5400 rpm', 'SATA', 1024, '2.5 inch')

INSERT INTO Memorii(mid, tip, dimensiune, frecventa) VALUES
(1, 'DDR4', '8 GB', '2133 MHz'),
(2, 'DDR4', '8 GB', '2400 MHz'),
(3, 'DDR3', '8 GB', '1600 MHz'),
(4, 'DDR4', '4 GB', '2133 MHz'),
(5, 'DDR4', '8 GB', '2666 MHz'),
(6, 'DDR4', '4 GB', '2400 MHz')

INSERT INTO Mouse(moid, nume, pret) VALUES 
(1, 'Microsoft Mobile 1850 for business Black', 59.99),
(2, 'Logitech B100 Optical USB', 31.98),
(3, 'SteelSeries Rival 110 Black', 185.98),
(4, 'Redragon Pegasus', 76.49),
(5, 'Logitech G205 Prodigy', 129.99)

INSERT INTO PlaciSunet(psid, sunet, numarCanale, sistem) VALUES
(1, 'HD Audio', 2.1, 'Stereo'),
(2, 'HD Audio', 2.0, 'Stereo')

INSERT INTO PlaciVideo(pvid, nume, tip, tipMemorie, dimensiuneMemorie) VALUES
(1, 'Intel HD 620', 'integrata', '-', '-'),
(2, 'Intel HD 6000', 'integrata', '-', '-'),
(3, 'nVidia GTX 1050 Ti', 'dedicata', 'GDDR5', '4 GB'),
(4, 'Intel HD 500', 'integrata', '-', '-'),
(5, 'nVidia GTX 1050', 'dedicata', 'GDDR5', '4 GB')

INSERT INTO SistemeOperare(soid, nume, tip) VALUES
(1, 'Windows 10 Home', 'Windows'),
(2, 'Windows 10 Student', 'Windows'),
(3, 'Windows 10 Pro', 'Windows'),
(4, 'macOS Sierra', 'macOS'),
(5, 'Ubuntu', 'Linux'),
(6, 'Endless OS', 'Linux'),
(7, 'Free DOS', 'DOS')

INSERT INTO SSD(ssdid, interfata, capacitate) VALUES
(1, 'M.2', 256),
(2, 'PCIe', 128),
(3, 'M.2', 128)


INSERT INTO Laptopuri(lid, nume, pret, pid, pvid, mid, ssdid, hid, psid, soid, oid) VALUES 
(1, 'Laptop HP 15.6" 250 G6', 2998.99, 1, 1, 1, 1, null, 1, 1, null)

INSERT INTO Laptopuri(lid, nume, pret, pid, pvid, mid, ssdid, hid, psid, soid, oid) VALUES 
(2, 'Laptop HP 15.6" 250 G6', 2798.99, 1, 1, 1, 1, null, 2, 7, null),
(3, 'Laptop Lenovo 15.6" V330 IKB', 2448.99, 2, 2, 1, 1, null, 2, null, null),
(4, 'Laptop Apple 13.3" MacBook Air 13', 4098.99, 3, 2, 3, 3, null, 2, 4, null),
(5, 'Laptop ASUS 15.6" VivoBook X541UA', 1698.99, 4, 1, 4, 1, null, 2, 6, null),
(6, 'Laptop ASUS Gaming 15.6" TUF FX504GE', 1698.99, 5, 3, 5, null, 1, 2, 7, null),
(7, 'Laptop HP 15.6" 450 G5', 3298.99, 6, 1, 2, 1, null, 2, 3, null),
(8, 'Laptop DELL 15.6" Inspiron 3567', 1498.99, 7, 4, 6, null, 1, 2, 5, null),
(9, 'Laptop HP 15.6" 250 G6', 1198.99, 8, 4, 6, 3, null, 2, 7, null),
(10, 'Laptop ASUS Gaming 15.6" ROG GL503VD', 3798.99, 9, 5, 2, 1, null, 2, null, null)

INSERT INTO Aplicatii(aid, nume, scop, spatiuNecesar) VALUES
(1, 'MATLAB', 'Matematica', 13),
(2, 'Visual Studio', 'Informatica', 30),
(3, 'CLion', 'Informatica', 5),
(4, 'PyCharm', 'Informatica', 7)

INSERT INTO aplicatiiCompatibile(lid, aid) VALUES 
(4, 1),
(2, 2),
(3, 4),
(10, 1),
(10, 2),
(10, 3)

INSERT INTO MouseCompatibil(lid, moid) VALUES 
(1, 1),
(2, 2),
(6, 3),
(6, 5),
(10, 3)

INSERT INTO Oferte(oid, lid, pret, rataLunara) VALUES
(1, 1, 2500, 500),
(2, 4, 3900, 600),
(3, 10, 3500, 750)

ALTER TABLE Procesoare ALTER COLUMN nucleu VARCHAR(20)
ALTER TABLE Procesoare ALTER COLUMN nume VARCHAR(50)
ALTER TABLE HDD ALTER COLUMN viteza VARCHAR(20)
ALTER TABLE HDD ALTER COLUMN tip VARCHAR(20)
ALTER TABLE HDD ALTER COLUMN capacitate float
ALTER TABLE Mouse ALTER COLUMN nume VARCHAR(50)
ALTER TABLE PlaciSunet ALTER COLUMN numarCanale float
ALTER TABLE Laptopuri DROP COLUMN oid
ALTER TABLE SSD ALTER COLUMN capacitate int

ALTER TABLE Laptopuri ALTER COLUMN nume varchar(50)

DROP TABLE Oferte

SELECT * FROM Procesoare

DELETE FROM HDD

DELETE FROM Procesoare

SELECT * FROM HDD

SELECT * FROM Memorii

SELECT * FROM Mouse

SELECT * FROM PlaciSunet

SELECT * FROM PlaciVideo

SELECT * FROM SistemeOperare

SELECT * FROM SSD


SELECT P.pid, P.nume, P.frecventa, L.lid, L.nume, L.pret, L.pid
FROM Procesoare P INNER JOIN Laptopuri L ON P.pid = L.pid
WHERE P.frecventa > 2.2


SELECT lid, nume, pret, ssdid
FROM Laptopuri
WHERE pret > 2000
GROUP BY lid

-- WHERE 2/5
-- GROUP BY 0/3
-- DISTINCT 0/2
-- HAVING 0/2
-- 2 TABELE 1/7
-- relatii m-n 0/2