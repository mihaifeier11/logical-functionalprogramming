; definiti o functie care elimina toate aparitiile unui element dintr-o lista neliniara.

;elimin(l1, ..., ln, e) = 
; 						0/, l = 0/ (0/ - lista vida)
;						l1 + elimin(l1, ..., ln, e), l1 != e si l1 - atom
;						elimin(l2, ..., ln, e), l1 = e
;						elimin(l1 + elimin(l2, ...n ln, e)), alftel

(defun elimin(l e)
	(cond
		((null l) nil)
		((= (car l) e)
			(elimin (cdr l) e))
		((atom (car l))
			(cons (car l)(elimin(cdr l) e)))

		(T (cons (elimin(car l) e)
			(elimin (cdr l) e)))
		)


	)

(defun lista(l)
	(cond
		((null l) nil)
		((atom (car l))
			(append (cons (car l) nil)(lista (cdr l))))
		((listp l)
			(append (lista (car l))(lista (cdr l))))
		))

; 15

(defun postorder(l)
	(cond
		((null l) nil)
		((AND (listp (car(cdr l))) (listp (cdr(cdr l))) )
			(append (postorder (car (cdr l))) (postorder (car(cdr (cdr l)))) (list(car l)) ))
		((OR (listp (car(cdr l))) (listp (cdr(cdr l))))
			(append (postorder (cdr l)) (list(car l))))
		(T
			(list l))
		))