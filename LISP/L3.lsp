(defun modifApar(l e l2)
	(cond
		((null l) nil)
		((equal l e) l2)
		((atom l) (list l))
		(t (list (apply #'append (mapcar (lambda (x) (modifApar x e l2)) l))))
	)
)

(defun wrapper(l e l2)
	(car (modifApar l e l2))
	)