(defun inserareElement(l e index)
	(cond
		((AND (null l) (= (mod index 2) 0) )
			(list e))
		((AND (null l) (= (mod index 2) 1) )
			nil)
		((AND (= (mod index 2) 0) (> index 0) )
			(cons e (cons (car l) (inserareElement (cdr l) e (+ index 1) ) ) )

			)
		(T
			(cons (car l) (inserareElement (cdr l) e (+ index 1) ) ) )

		))

(defun inversareListaNeliniara(l)
	(cond
		((null l) nil)
		((atom (car l))
			(append (inversareListaNeliniara(cdr l)) (list(car l)) ) )
		(T
			(append (inversareListaNeliniara(cdr l)) (inversareListaNeliniara(car l) ) )
			)
		))

(defun cmmdc(a b)
	(cond
		((> a b)
			(cmmdc (- a b) b) )
		((> b a)
			(cmmdc a (- b a) ) )
		(T
			a)
		))

(defun cmmdcListaNeliniara(l val)
	(cond
		((AND (null (cdr l)) (atom (car l)))
			(cmmdc val (car l) ) ) 
		((null (cdr l))
			(cmmdcListaNeliniara(car l) val) ) 
		((atom (car l))
			(cmmdcListaNeliniara (cdr l) (cmmdc val (car l))) )
		(T
			(cmmdcListaNeliniara (cdr l) (cmmdcListaNeliniara (car l) val) ) )

		))

(defun cmmdcListaNeliniaraWrapper(l) 
	(cond
		(T
			(cmmdcListaNeliniara(cdr l) (primulElementAlListei(car l) ) ) )
		))

(defun primulElementAlListei(l)
	(cond
		((atom l)
			l )
		(T
			(primulElementAlListei(car l)))
		))

(defun numarAparitiiElementListaNeliniara(l e)
	(cond
		((null l) 0)
		((AND (atom (car l)) (= (car l) e))
			(+ 1 (numarAparitiiElementListaNeliniara (cdr l) e ) ) )
		((atom (car l))
			(numarAparitiiElementListaNeliniara (cdr l) e ) )
		(T
			(+ (numarAparitiiElementListaNeliniara (car l) e) (numarAparitiiElementListaNeliniara (cdr l) e ) ) )
		))

(defun elementulDePeOPozitie(l poz i)
	(cond
		((null l) nil)
		((= poz i)
			(car l) )
		(T
			(elementulDePeOPozitie(cdr l) poz (+ i 1)))
		))

(defun elementulDePeOPozitieWrapper(l poz)
	(cond
		(T
			(elementulDePeOPozitie l poz 1))
		))
(defun eMembruAlListei(l e)
	(cond
		((null l) 0)
		((AND (atom (car l)) (= (car l) e) )
			1)
		((atom (car l))
			(eMembruAlListei (cdr l) e))
		((OR (= (eMembruAlListei (car l) e) 1) (= (eMembruAlListei (cdr l) e) 1) )
			1)
		(T
			0)
		))

(defun toateSublisteleUneiListe(l)
	(cond
		((null l) nil)
		((listp (car l))
			(cons (car l) (toateSublisteleUneiListe (cdr l))))
		(T
			(toateSublisteleUneiListe (cdr l)))
		))

(defun toateSublisteleUneiListeWrapper(l)
	(cond
		(T
			(cons l (toateSublisteleUneiListe l)))
		))

(defun multimeDinLista(l)
	(cond
		((null l) nil)
		((AND (atom (car l) ) (= (eMembruAlListei (cdr l) (car l) ) 0) ) 
			(append (list (car l)) (multimeDinLista (cdr l)) ))
		((atom (car l))
			(multimeDinLista(cdr l)))
		(T
			(append (multimeDinLista (car l)) (multimeDinLista (cdr l))))
		))

(defun inversLista(l)
	(cond
		((null l) nil)
		(T
			(append (inversLista(cdr l)) (list (car l))))
		))

(defun sumaVectori(l1 l2)
	(cond
		((AND (null l1) (null l2))
			nil)
		((null l1) 
			(append (list (car l2)) (sumaVectori l1 (cdr l2)) ) )
		((null l2)
			(append (list (car l1)) (sumaVectori (cdr l1) l2)))
		(T
			(append (list (+ (car l1) (car l2) ) ) (sumaVectori (cdr l1) (cdr l2))))

		))

(defun listaLiniaraDinListaNeliniara(l)
	(cond
		((null l) nil)
		((atom (car l))
			(append (list(car l)) (listaLiniaraDinListaNeliniara (cdr l))))
		(T
			(append (listaLiniaraDinListaNeliniara(car l)) (listaLiniaraDinListaNeliniara (cdr l))))
		))

(defun maxim(l m)
	(cond
		((null l) m)
		((AND (numberp (car l)) (> (car l) m))
			(maxim (cdr l) (car l)))
		(T
			(maxim (cdr l) m))
		))

(defun test(i)
	(cond
		((= (mod i 2) 0)
			1)
		(T
			2)
		))