;(inserare '1 '(2 3)) -> ((1 2 3) (2 1 3) (2 3 1))

;functia insereaza pe fiecare pozitie a listei elementul
(defun inserare(e l index len)
	(cond
		((< len index) nil)
		(T
			(cons (inserarePoz e l index 1) (inserare e l (+ index 1) len) ) )
		))

;functia insereaza pe pozitia data un element
(defun inserarePoz(e l poz index)
	(cond
		((AND (null l) (= poz index))
			(list e))
		((null l) nil)
		((= poz index) 
			(append (list e) (inserarePoz e l poz (+ index 1))))
		(T
			(append (list(car l))  (inserarePoz e (cdr l) poz (+ index 1))) )
		))

;functia calculeaza lungimea listei
(defun lenLista(l)
	(cond
		((null l) 0)
		(T
			(+ 1 (lenLista (cdr l))))
		))

;functia de Wrapper pentru functia de inserare
(defun inserareWrapper(e l)
	(cond
		(T
			(inserare e l 1 (+ 1 (lenLista l))))
		))