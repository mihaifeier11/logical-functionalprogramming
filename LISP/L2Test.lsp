;(1(2(4 5) ) (3(6 7(8) ) ) )
;(1 (2) (3))

(defun inorder(l)
	(cond
		((null l) nil)
		((AND (listp (car(cdr l))) (listp (cdr(cdr l))) )
			(append (inorder (car (cdr l))) (list(car l)) (inorder (car(cdr (cdr l))))  ))
		((OR (listp (car(cdr l))) (listp (cdr(cdr l))))
			(append (inorder (cdr l)) (list(car l))))
		(T
			(list l))
		))

(defun postorder(l)
	(cond
		((null l) nil)
		((AND (listp (car(cdr l))) (listp (cdr(cdr l))) )
			(append (postorder (car (cdr l))) (postorder (car(cdr (cdr l)))) (list(car l)) ))
		((OR (listp (car(cdr l))) (listp (cdr(cdr l))))
			(append (postorder (cdr l)) (list(car l))))
		(T
			(list l))
		))


(defun preorder(l)
	(cond
		((null l) nil)
		((AND (listp (car(cdr l))) (listp (cdr(cdr l))) )
			(append (list(car l)) (preorder (car (cdr l))) (preorder (car(cdr (cdr l))))  ))
		((OR (listp (car(cdr l))) (listp (cdr(cdr l))))
			(append (list(car l)) (preorder (cdr l)) ))
		(T
			(list l))
		))

(defun appLevel(l e lvl)
	(cond
		((null l) nil)
		((equal (car l) e)
			lvl)
		((AND (listp (car(cdr l))) (listp (cdr(cdr l))) )
			(appLevel (car (cdr l)) e (+ lvl 1)) 
			(appLevel (car(cdr (cdr l))) e (+ lvl 1) ) )
		((OR (listp (car(cdr l))) (listp (cdr(cdr l))))
			(appLevel (cdr l) e (+ lvl 1)) )
		(T
			(list l))
		))

(defun maxNodes(l clvl lvl ln m)
	(cond
		((null l) )
			;(princ lvl)
			;(princ ln))
		((AND (AND (listp (car(cdr l))) (listp (cdr(cdr l))) (> 2 m)) )
			(princ lvl)
			(maxNodes (car (cdr l)) (+ clvl 1) clvl (cdr l) 2)
			(maxNodes (car (cdr(cdr l))) (+ clvl 1) clvl (cdr l) 2) )
		((AND(OR (listp (car(cdr l))) (listp (cdr(cdr l)))) (> 1 m))
			(maxNodes (cdr l) (+ clvl 1) clvl (car l) 1) )
		(T
			(maxNodes (cdr l) (+ clvl 1) lvl ln m) )
		))

(defun cale(l e lc)
	(cond
		((null l) nil)
		((equal (car l) e) lc)
		((AND (listp (car(cdr l))) (listp (cdr(cdr l))) )
			(cale (car (cdr l)) e (append lc (list(car l)))) 
			(cale (car(cdr (cdr l) ) ) e (append lc (list(car l)))))
		((OR (listp (car(cdr l))) (listp (cdr(cdr l))))
			(cale (cdr l) e (list(car l))) )
		(T
			(list l))
		))

(defun adancime(l)
	(cond
		((null l) 0)
		((AND (listp (car(cdr l))) (listp (cdr(cdr l))) )
			(+ 1 (adancime (car (cdr l))) )
			(+ 1 (adancime (car(cdr (cdr l) ) ))))
		((OR (listp (car(cdr l))) (listp (cdr(cdr l))))
			(+ 1 (adancime (cdr l) ) ))
		(T
			0)
		))

(defun echilibrat(l)
	(cond
		((AND (>(- (adancime (car (cdr l))) (adancime (car(cdr (cdr l)))) ) -2) (<(- (adancime (car (cdr l))) (adancime (car(cdr (cdr l)))) ) 2))
			1)
		(T 
			0)
		))