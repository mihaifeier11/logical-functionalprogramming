% 1.a
foundInList([], _, 0).
foundInList([H|_], E, 1):-
    H=:=E.
foundInList([_|T], E, R):-
    foundInList(T, E, R).

setDifference([], _, []).
setDifference([H|T], L, LR):-
    foundInList(L, H, 1),!,
    setDifference(T, L, LR).
setDifference([H|T], L, [H|LR]):-
    setDifference(T, L, LR).

%1.b
addNumberAfterEvenIndex([], []).
addNumberAfterEvenIndex([H|T], [H,1|LR]):-
    mod(H, 2) =:= 0,!,
    addNumberAfterEvenIndex(T, LR).
addNumberAfterEvenIndex([H|T], [H|LR]):-
    addNumberAfterEvenIndex(T, LR).

% 2.a
cmmdc(A, B, A):-
    A=:=B.
cmmdc(A, B, R):-
    A>B,!,
    A1 is A-B,
    cmmdc(A1, B, R).
cmmdc(A, B, R):-
    A<B,!,
    B1 is B-A,
    cmmdc(A, B1, R).

cmmmc(A, B, R):-
    cmmdc(A,B, C),
    R is (A*B)/C.

cmmmcList([], R, R).
cmmmcList([H|T], C, R):-
    cmmmc(H, C, C1),!,
    cmmmcList(T, C1, R).

cmmmcListWrapper([H|T], R):-
    cmmmcList(T, H, R).

%2.b
po2(N, P, 1):-
    N =:= P.
po2(N, P, 0):-
    N < P.
po2(N, P, R):-
    N > P,
    P1 is P*2,
    po2(N, P1, R).

addAfterPowerOf2Index([], _, _, []).
addAfterPowerOf2Index([H|T], E, I, [H, E|LR]):-
    po2(I,1, 1),!,
    I1 is I+1,
    addAfterPowerOf2Index(T, E, I1, LR).
addAfterPowerOf2Index([H|T], E, I, [H|LR]):-
    I1 is I+1,
    addAfterPowerOf2Index(T, E, I1, LR).

%3.a

deleteElementFromList([], _, []).
deleteElementFromList([H|T], E, LR):-
    H=:=E,!,
    deleteElementFromList(T, E, LR).
deleteElementFromList([H|T], E, [H|LR]):-
    deleteElementFromList(T, E, LR).

% colocviu
% 2
deleteElementsPowerOf2Index([], _, []).
deleteElementsPowerOf2Index([_|T], I, LR):-
    po2(I,2, 1),!,
    I1 is I+1,
    deleteElementsPowerOf2Index(T, I1, LR).
deleteElementsPowerOf2Index([H|T], I, [H|LR]):-
    I1 is I+1,
    deleteElementsPowerOf2Index(T, I1, LR).

%3
candidat([H|_], H).
candidat([_|T], R):-
    candidat(T, R).

getAll([], []).
getAll([H|T], LP):-
    candidat(L, R1),
    candidat(L, R2),
    LP = [R1, R2|[]].





