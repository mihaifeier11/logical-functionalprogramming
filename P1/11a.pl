%apare(E:element,L:lista,Nr:int)
%E-elementul pe care il cautam in lista
%Nr-numarul de aparitii al lui E
%L- lista in care cautam
%model de flux (i,i,o)
Apare(_,[],0).
apare(E,[H|T],Nr):-H=E,!,apare(E,T,Nr1),Nr is Nr1+1.
apare(E,[H|T],Nr):-H\=E,apare(E,T,Nr).

%adaugaSf(E:element,L:lista,Lr:lista)
%E-elementul care se adauga la sf listei
%L- lista in care se adauga
%Lr-lista rezultat
%modelul de flux (i,i,o)
adaugaSf(_,[],[]).
adaugaSf(E,[H|T],Lr):-adaugaSf(E,T,Lr).

%multime(L:lista, Ma:lista, M:lista)
%L - lista care trebuie transformata
%Ma - lista in care punem elementele din multime
%M - multimea rezultata
%model de flux (i,i,o)
multime([],_,[]).
multime([H|T],Ma,M):-apare(H,Ma,1),multime(T,Ma,M),!.
multime([H|T],Ma,[H|M]):-not(apare(H,Ma,1)),multime(T,[H|Ma],M).

%mutlimea(L:lista,M:lista)
%L:lista care trebuie transformata
%M: multimea rezultata
%model de flux(i,o)

multimea(L,M):-multime(L,[],M).
