% candidat(L: lista, E: char)
% model de flux(i, o)
% L - lista din care se returneaza un candidat
% E - candidatul
candidat([H|_], H).
candidat([_|T], R):-
    candidat(T,R).

% verificare(L: lista, E: int, R: int)
% model de flux(i, i, o)
% L - lista unei posibile solutii
% E - numarul de "X"-uri aparute
% R - rezultatul verificarii (0/1)
verificare([], E, R):-
    E > 2,!,
    R is 0.
verificare([], _, R):-
    R is 1.
verificare([H], _, R):-
    H =:= "2",!,
    R is 0.
verificare([H|T], E, R):-
    H =:= "X",!,
    E1 is E+1,
    verificare(T, E1, R).
verificare([_|T], E, R):-
    verificare(T, E, R).

% getAll(L: lista, LP: lista)
% model de flux(i, o)
% L - lista de elemente
% LP - lista formata din 4 elemente dintre cele din L
getAll(L, LP):-
    candidat(L, P1),
    candidat(L, P2),
    candidat(L, P3),
    candidat(L, P4),
    LP = [P1,P2,P3,P4|[]].

% verificareLista(L)
% model de flux(i)
% L - lista ce trebuie verificata
verificareLista([]).
verificareLista([H|T]):-
    verificare(H, 0, 1),!,
    write(H),
    nl(),
    verificareLista(T).
verificareLista([_|T]):-
    verificareLista(T).

% pronosticuri()
% model de flux()
pronosticuri():-
    L = ["1", "X", "2"|[]],
    findall(LP, getAll(L, LP), R),
    verificareLista(R).
