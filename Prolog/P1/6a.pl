eliminare([], _, []).
eliminare([_|T], N, LR):-
    N =:= 1,!,
    N1 is N - 1,
    eliminare(T, N1, LR).
eliminare([H|T], N, [H|LR]):-
    N1 is N-1,
    eliminare(T, N1, LR).

nrAparitii([], _, 0).
nrAparitii([H|T], E, NR1):-
    H =:= E,!,
    nrAparitii(T, E, NR),
    NR1 is NR + 1.
nrAparitii([_|T], E, NR):-
    nrAparitii(T, E, NR).

eliminaRepetitii([], []).
eliminaRepetitii([H|T], [H|LR]):-
    nrAparitii(T, H, 0),!,
    eliminaRepetitii(T, LR).
eliminaRepetitii([H|T], LR):-
    eliminare(T, H, T1),
    eliminaRepetitii(T1, LR).
