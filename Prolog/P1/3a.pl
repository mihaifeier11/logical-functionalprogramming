apare(_,[],0).
apare(E,[H|T],Nr):-
    H=E,!,
    apare(E,T,Nr1),
    Nr is Nr1+1.
apare(E,[H|T],Nr):-
    H\=E,
    apare(E,T,Nr).

set([], []).
set([H|T], Rez):-
    apare(H, LR, 0),!,
    set(T, [H|LR]),
    Rez = [H|LR].

set([_|T], LR):-
    set(T, LR).



l2s([], [], []).
l2s([H|T], Rez, [H|Lr]):-
   apare(H, Lr, 0),!,
   l2s(T, CL, [H|Lr]),
   Rez = [H|Lr].
l2s([H|T], CL, Lr):-
    apare(H, Lr, 1),
    l2s(T, CL, Lr).
