eliminare([], _, []).
eliminare([_|T], N, LR):-
    N =:= 1,!,
    N1 is N - 1,
    eliminare(T, N1, LR).
eliminare([H|T], N, [H|LR]):-
    N1 is N-1,
    eliminare(T, N1, LR).
