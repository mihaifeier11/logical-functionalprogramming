powerof(1, _, R):-
    R is 1.
powerof(E, C, R):-
    E = C,!,
    R is 1.
powerof(E, C, R):-
    E > C,!,
    C1 is C*2,
    powerof(E, C1, R).
powerof(_, _, R):-
    R is 0.


add([], _, _, []).
add([H|T], E, Poz, [H,E|LR]):-
    powerof(Poz, 2, 1),!,
    Poz1 is Poz + 1,
    add(T, E, Poz1, LR).
add([H|T], E, Poz, [H|LR]):-
    Poz1 is Poz + 1,
    add(T, E, Poz1, LR).

addWrapper(L, E, LR):-
    add(L, E, 1, LR).
