eliminareNr([], _, []).
eliminareNr([H|T], E, LR):-
    H =:= E,!,
    eliminareNr(T, E,LR).
eliminareNr([H|T], E, [H|LR]):-
    eliminareNr(T, E, LR).
