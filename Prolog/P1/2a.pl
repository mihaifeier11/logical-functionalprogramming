cmmdc(A, B, R):-
    A = B,!,
    R is A.
cmmdc(A, B, R):-
    A > B,!,
    A1 is A - B,
    cmmdc(A1, B, R).
cmmdc(A, B, R):-
    B1 is B - A,
    cmmdc(A, B1, R).

cmmmc(A, B, R):-
    cmmdc(A, B, RP),
    R is A*B/RP.


cmmmcLista([], C, C).
cmmmcLista([H|T], C, R):-
    cmmdc(H, C, C1),
    cmmmcLista(T, C1, R).

cmmmcListaWrapper(L, R):-
    cmmmcLista(L, 1, R).
