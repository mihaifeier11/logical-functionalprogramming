nrAparitii([], _, 0).
nrAparitii([H|T], E, NR1):-
    H =:= E,!,
    nrAparitii(T, E, NR),
    NR1 is NR + 1.
nrAparitii([_|T], E, NR):-
    nrAparitii(T, E, NR).

eliminareNr([], _, []).
eliminareNr([H|T], E, LR):-
    H =:= E,!,
    eliminareNr(T, E,LR).
eliminareNr([H|T], E, [H|LR]):-
    eliminareNr(T, E, LR).


listaPerechi([], []).
listaPerechi([H|T], [S|LR]):-
    nrAparitii([H|T], H, R),
    S = [H,R|[]],
    eliminareNr(T, H, T1),
    listaPerechi(T1, LR).
