adaugare([], []).
adaugare([H|T], [H,R|LR]):-
    mod(H, 2) =:= 0,!,
    R is 1,
    adaugare(T, LR).
adaugare([H|T], [H|LR]):-
    adaugare(T, LR).
