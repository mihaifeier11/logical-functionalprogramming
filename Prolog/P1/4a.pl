substituire([],_,_,[]).
substituire([H|T],E,L, R):-
    H =:= E,!,
    substituire(T,E, L, LR),
    R = [L|LR].
substituire([H|T], E, L, R):-
    substituire(T, E, L, LR),
    R = [H|LR].
