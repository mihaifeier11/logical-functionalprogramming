gasit(_, [], 0).
gasit(E, [H|_], R):-
    E =:= H,!,
    R is 1.
gasit(E, [_|T], R):-
    gasit(E, T, R).

diferenta([], _, []).
diferenta([H1|T1], L2, LR):-
    gasit(H1, L2, 1),!,
    diferenta(T1, L2, LR).
diferenta([H1|T1], L2, [H1|LR]):-
    diferenta(T1, L2, LR).

