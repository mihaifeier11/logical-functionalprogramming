insert1([], E, [E]).
insert1([H|T], E, [E, H|T]).
insert1([H|T], E, [H|R]):-
    insert1(T, E, R).

perm1([], []).
perm1([H|T], R):-
    perm1(T, RT),
    insert1(RT, H, R).


consecutiveSum([H1, H2], 1):-
    abs(H1 - H2) =< 3,!.
consecutiveSum([_, _], 0).
consecutiveSum([H1, H2|T], R):-
    abs(H1 - H2) =< 3,!,
    consecutiveSum([H2|T], R).
consecutiveSum(_, 0).


oneSol(L, LR):-
    perm1(L, R),
    consecutiveSum(R, 1),
    LR = R.

insert2([], E, [E]).
insert2([H|T], E, [E, H|T]).
insert2([H|T], E, [H|LR]):-
    insert2(T, E, LR).

perm2([], []).
perm2([H|T], R):-
    perm2(T, RT),
    insert2(RT, H, R).

comb2(_, 0, []).
comb2([H|T], K, [H|R]):-
    K > 0,
    K1 is K - 1,
    comb2(T, K1, R).
comb2([_|T], K, R):-
    K > 0,
    comb2(T, K, R).

arr2(L, K, LR):-
    comb2(L, K, R),
    perm2(R, LR).

sum2([], 0).
sum2([H|T], S1):-
    sum2(T, S),
    S1 is S + H.

oneSol2(L, K, S, LR):-
    arr2(L, K, R),
    sum(R, S),
    LR = R.
