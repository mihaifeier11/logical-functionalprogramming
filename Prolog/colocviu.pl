deleteFromList([], _, []).
deleteFromList([H|T], E, LR):-
    H =:= E,!,
    deleteFromList(T, E, LR).
deleteFromList([H|T], E, [H|LR]):-
    deleteFromList(T, E, LR).

list2set([], []).
list2set([H|T], [H|LR]):-
    deleteFromList(T, H, T1),
    list2set(T1, LR).

foundInList([], _, 0).
foundInList([H|_], E, 1):-
    H =:= E,!.
foundInList([_|T], E, R):-
    foundInList(T, E, R).

list2set2([], _, []).
list2set2([H|T], LR, [H|Rez]):-
    foundInList(LR, H, 0),!,
    list2set2(T, [H|LR], Rez).
list2set2([H|T], LR, Rez):-
    foundInList(LR, H, 1),!,
    list2set2(T, LR, Rez).

subsets([], []).
subsets([H|T], R):-subsets(T, RT),
    R = [H|RT].
subsets([_|T], R):-subsets(T,R).

candidat([H|_], H).
candidat([_|T], R):-
    candidat(T,R).

minsert([], E, [E]).
minsert([H|T], E, R):-
    R = [E, H | T].
minsert([H|T], E, R):-
    minsert(T, E, RT),
    R = [H|RT].

perm([], []).
perm([H|T], R):-perm(T, RT),
    minsert(RT, H, R).

comb(_, 0, []).
comb([H|T], K, R):- K > 0,
    K1 is K-1,
    comb(T, K1, RT),
    R = [H|RT].
comb([_|T], K, R):- K > 0,
    comb(T, K, RT),
    R = RT.

arrang(L, K, R):- comb(L, K, RC),
    perm(RC, R).

product([], 1).
product([H|T], R):-product(T, RT),
    R is H * RT.

%oneSol(L, K, P, R):- arrang(L, K, RA),
%    product(RA, P),
%    R = RA.

allSol(L, K, P, R):-findall(RO, oneSol(L, K, P, RO), R).

insert([], E, [E]).
insert([H|T], E, [E,H|T]).
insert([H|T], E, [H|RT]):-
    insert(T, E, RT).

perm1([], []).
perm1([H|T], R):-
    perm1(T, RT),
    insert(RT, H, R).

comb1(_, 0, []).
comb1([H|T], K, [H|R]):-
    K > 0,
    K1 is K - 1,
    comb1(T, K1, R).
comb1([_|T], K, R):-
    K > 0,
    comb1(T, K, R).

arr(L, R, K):-
    comb1(L, K, R1),
    perm(R1, R).

sum([], 0).
sum([H|T], S1):-
    sum(T, S),
    S1 is S + H.

oneSol(L, K, S, R):-
    arr(L, R1, K),
    sum(R1, S),
    R = R1.

pow1(_, 1, C, C).
pow1(N, P, R, C):-
    R1 is R * N,
    P1 is P - 1,
    pow1(N, P1, R1, C).

pow2(_, 0, 1).
pow2(N, P, R):-
    P1 is P - 1,
    pow2(N, P1, R1),
    R is R1 * N.

insert1([], E, [E]).
insert1([H|T], E, [E, H|T]).
insert1([H|T], E, [H|R]):-
    insert1(T, E, R).

perm1([], []).
perm1([H|T], R):-
    perm1(T, RT),
    insert1(RT, H, R).


consecutiveSum([_], 1).
consecutiveSum([H1, H2|T], R):-
    H1 - H2 =< 3,!,
    consecutiveSum([H2|T], R).
consecutiveSum(_, 0).




























