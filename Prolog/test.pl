% model de flux(i, i, o)
maximum([], C, C).
maximum([H|T], C, R):-
    H > C,!,
    maximum(T, H, R).
maximum([_|T], C, R):-
    maximum(T, C, R).

% model de flux(i, i, o)
add_last_list([], L, L).
add_last_list([H|T], L, [H|LR]):-
    add_last_list(T, L, LR).

% model de flux(i, i, o)
add_last([], E, [E|[]]).
add_last([H|T], L, [H|LR]):-
    add_last(T, L, LR).

% model de flux(i, i, i, i, o)
inlocuire([], _, _, C, C).
inlocuire([H|T], L, E, C, R):-
    H =:= E,!,
    add_last_list(C, L, C1),
    inlocuire(T, L, E, C1, R).
inlocuire([H|T], L, E, C, R):-
    add_last(C, H, C1),
    inlocuire(T, L, E, C1, R).

% model de flux(i, i, o)
inlocuireWrapper(L, LI, LR):-
    maximum(L, -2001, R),
    inlocuire(L, LI, R, [], LR).
