maximum([], C, C).
maximum([H|T], M, R):-
    H > M,
    maximum(T, H, R).
maximum([_|T], M, R):-
    maximum(T, M, R).


aparitii_maxim([], _,_, []).
aparitii_maxim([H|T], M, C, [C|LR]):-
    H =:= M,
    C1 is C+1,
    aparitii_maxim(T, M, C1, LR).
aparitii_maxim([_|T], M, C, LR):-
    C1 is C+1,
    aparitii_maxim(T, M, C1, LR).

aparitii(L, LR):-
    maximum(L, 0, M),
    aparitii_maxim(L, M, 1, LR).
