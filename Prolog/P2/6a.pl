add_lastL([], E, E).
add_lastL([H|T], E, [H|LR]):-
    add_lastL(T, E, LR).

add_last([], E, [E|[]]).
add_last([H|T], E, [H|LR]):-
    add_last(T, E, LR).


concatenare([], [], []).
concatenare([H|T], L, [H|R]):-
    concatenare(T, L, R).
concatenare([], [H|T], [H|R]):-
    concatenare([], T, R).


inlocuire([], _, _, C, C).
inlocuire([H|T], L, E, C, R):-
    H =:= E,!,
    add_lastL(C, L, C1),
    inlocuire(T, L, E, C1, R).
inlocuire([H|T], L, E, C, R):-
    H \= E,
    add_last(C, H, C1),
    inlocuire(T, L, E, C1, R).
