--Cerinte Fallout 4
--OS: Windows 7/8/10 (64-bit OS required) // done
--Processor: Intel Core i7 4790 2.6 GHz/AMD FX-9590 3.7 GHz or equivalent  // done
--Graphics Card: NVIDIA GTX 780 3GB/AMD Radeon R9 290X 4GB or equivalent  // done
--Memory: 8 GB RAM // done
--Storage: 30 GB free HDD space // done

--Prima cerinta, tipul sistemului de operare trebuie sa fie Windows, iar versiunea trebuie sa fie cel putin 7
--A 2-a cerinta, numele procesorului trebuie sa contina 'i7-4' (i7, a 4-a generatie) sau 'i5-6' 
--	( i5, a 6-a generatie - aprox. echivalent cu i7, a 4-a generatie), iar frecventa procesorului sa fie mai mare sau egala cu 2.6GHz
--A 3-a cerinta, daca producatorul placii video este nVidia, dimensiunea a placii video trebuie sa fie cel putin 3 GB, 
--	iar daca placa video este AMD, dimensiunea placii video trebuie sa fie de cel putin 4GB
--A 4-a cerinta, dimensiunea memoriei RAM trebuie sa fie cel putin 8 GB
--A 5-a cerinta, spatiul liber (spatiul SSD-ului - spatiul sistemului de operare, daca exista) trebuie sa fie minim 30 GB

SELECT L.lid, L.nume, L.pret, L.soid, L.pid, L.pvid, L.mid, L.ssdid,
	SO.soid, SO.versiune, SO.tip, SO.spatiuNecesarGB,
	P.pid, P.nume, P.frecventaGHz,
	PV.pvid, PV.producator, PV.dimensiuneMemorieGB,
	M.mid, M.dimensiuneGB,
	SSD.ssdid, SSD.capacitate
FROM Laptopuri L INNER JOIN SistemeOperare SO ON L.soid = SO.soid
	INNER JOIN Procesoare P ON L.pid = P.pid
	INNER JOIN PlaciVideo PV ON L.pvid = PV.pvid
	INNER JOIN Memorii M ON L.mid = M.mid
	LEFT OUTER JOIN SSD SSD ON L.ssdid = SSD.ssdid
WHERE SO.tip = 'Windows' AND SO.versiune >= 7
	AND (P.nume LIKE '%i7-4%'
		OR P.nume LIKE '%i7-5%'
		OR P.nume LIKE '%i7-6%'
		OR P.nume LIKE '%i7-7%'
		OR P.nume LIKE '%i7-8%'
		OR P.nume LIKE '%i5-6%'
		OR P.nume LIKE '%i5-7%'
		OR P.nume LIKE '%i5-8%')
	AND P.frecventaGHz > 2.6
	AND ((PV.producator = 'nVidia' AND PV.dimensiuneMemorieGB > 3) OR
		(PV.producator = 'AMD' AND PV.dimensiuneMemorieGB > 4))
	AND M.dimensiuneGB >= 8
	AND (SSD.capacitate - SO.spatiuNecesarGB) > 30
		
ALTER TABLE Memorii ALTER COLUMN dimensiuneGB float
