USE Ceainarie
GO
INSERT INTO Marci(Nume, Rating) VALUES ('Fares', 1), ('Lipton', 2)

SELECT * FROM Marci

INSERT INTO Ceaiuri(Cid, Nume, Temperatura, Mid) VALUES (1, 'Verde', 99, 1)
INSERT INTO Ceaiuri VALUES (2, 'Negru', 25, 1)

SELECT * FROM Ceaiuri

INSERT INTO Ingrediente VALUES('Apa', 'Robinet', 100), ('Tei', 'Copac', 50)
SELECT * FROM Ingrediente

INSERT INTO Continuturi(Cid, Iid, CantitateC) VALUES (1, 1, 100), (1, 2, 25)
SELECT * FROM Continuturi

SELECT *
FROM Marci M, Ceaiuri C
WHERE M.Mid = C.Mid

SELECT *
FROM Marci M INNER JOIN Ceaiuri C ON M.Mid = C.Mid

SELECT *
FROM Marci M LEFT OUTER JOIN Ceaiuri C ON M.Mid = C.Mid

SELECT *
FROM Marci M RIGHT OUTER JOIN Ceaiuri C ON M.Mid = C.Mid

SELECT *
FROM Marci M FULL JOIN Ceaiuri C ON M.Mid = C.Mid

SELECT C.Nume, I.Nume
FROM Ceaiuri C INNER JOIN Continuturi Co ON C.Cid = Co.Cid
INNER JOIN Ingrediente I ON Co.Iid = I.Iid AND Temperatura <= 50
WHERE C.Nume LIKE 'A%'

--Sa se afiseze pt fiecare marca temperatura medie a ceaiurilor

SELECT M.Mid, AVG(Temperatura) AS TempMedie
FROM Marci M INNER JOIN Ceaiuri C ON M.Mid = C.Mid
GROUP BY M.Mid
HAVING AVG(Temperatura)> 50 AND (Marci.Mid <= 10)