nr_from_list([], C, C).
nr_from_list([H|T], C, R):-
    C1 is C*10 + H,
    nr_from_list(T, C1, R).

list_from_nr(0, C, C).
list_from_nr(Nr, L, R):-
    TN is mod(Nr, 10),
    Nr1 is div(Nr, 10),
    list_from_nr(Nr1, [TN|L], R).

sum(L1, L2, LR):-
    nr_from_list(L1, 0, A),
    nr_from_list(L2, 0, B),
    S is A + B,
    list_from_nr(S, [], LR).
