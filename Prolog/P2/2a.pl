add_last([], E, [E|[]]).
add_last([H|T], E, [H|LR]):-
    add_last(T, E, LR).

minimum([], C, C).
minimum([H|T], C, R):-
    H < C,!,
    C1 is H,
    minimum(T, C1, R).
minimum([_|T], C, R):-
    minimum(T, C, R).

concatenare([], [], []).
concatenare([H|T], L, [H|R]):-
    concatenare(T, L, R).
concatenare([], [H|T], [H|R]):-
    concatenare([], T, R).

remove_all_mins([], _, []).
remove_all_mins([H|T], E, R):-
    H =:= E,
    remove_all_mins(T, E, R).
remove_all_mins([H|T], E, [H|R]):-
    remove_all_mins(T, E, R).


add_all_mins([], _, [], []).
add_all_mins([H|T], E, Remaining, LR1):-
    H =:= E,!,
    add_all_mins(T, E, Remaining, LR),
    add_last(LR, H, LR1).
add_all_mins([H|T], E, [H|Remaining], LR):-
    add_all_mins(T, E, Remaining, LR).

sorting([], C, C).
sorting(L, C, R):-
    minimum(L, 999999, EM),!,
    add_all_mins(L, EM, Remaining, LR1),
    concatenare(C, LR1, LRF),
    sorting(Remaining, LRF, R).


sorting2([], C, C).
sorting2(L, C, R):-
    minimum(L, 999999, EM),!,
    remove_all_mins(L, EM, L1),
    add_last(C, EM, C1),
    sorting(L1, C1, R).




sortingWrapper(L, R):-
    sorting(L, [], R).
