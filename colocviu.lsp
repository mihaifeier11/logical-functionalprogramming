(defun removeNthElement(l n i)
	(cond
		((null l) nil)
		((=(mod i n) 0)
			(removeNthElement(cdr l) n (+ i 1)))
		(T
			(append (list(car l)) (removeNthElement (cdr l) n (+ i 1))))

	))

(defun nonnumericAtoms(l)
	(cond
		((null l) nil)
		((AND(atom l) (not(numberp l)))
			1)
		((atom l)
			0)
		(T
			(apply '+ (mapcar (lambda (x) (nonnumericAtoms x)) l)))
		))


(defun main(l i)
	(cond
		((atom l)
			0)
		((AND(= (mod i 2) 0) (= (mod (nonnumericAtoms l) 2) 1)) 
			(princ l)
			(princ #\linefeed)
			(+ 1 (apply '+ (mapcar (lambda (x) (main x (+ i 1))) l))))
		
		(T
			(apply '+ (mapcar (lambda (x) (main x (+ i 1))) l)))
	)
)


(defun modifApar(l e l2)
	(cond
		((null l) nil)
		((equal l e) l2)
		((atom l) (list l))
		(t (list (apply #'append (mapcar (lambda (x) (modifApar x e l2)) l))))
	)
)

(defun maxNr(l)
	(cond
		((null l) 
			-9999)
		((numberp l) 
			l)
		((atom l) 
			-9999)
		(T
			(apply 'max (mapcar 'maxNr l) ))
	)
)


(defun main7(l i)
	(cond
		((null l) nil)
		((atom l)
			0)
		((AND (= (mod i 2) 1) (= (mod (maxNr l) 2) 0))
			(+ 1 (apply '+ (mapcar (lambda (x) (main7 x (+ i 1))) l))))
		(T
			(apply '+ (mapcar (lambda (x) (main7 x (+ i 1))) l)))
		))

(defun lastNrAtom(l n)
	(cond
		((null l) n)
		((numberp (car l)) (lastNrAtom (cdr l) (car l)))
		((listp (car l))
			(lastNrAtom (cdr l) (lastNrAtom (car l) n)))
		(T
			(lastNrAtom (cdr l) n))

		))

(defun mainR10(l)
	(cond
		((null l) 0)
		((atom l) 0)
		((AND (not (=(lastNrAtom l -20000) -20000)) (=(mod (lastNrAtom l -20000) 2) 1) )
			(+ 1 (apply '+ (mapcar 'mainR10 l) ) ) )
		(T
			(apply '+ (mapcar 'mainR10 l) ))
	))

(defun sumList(l)
	(cond
		((null l) 0)
		((numberp (car l)) 
			(+ (car l) (sumList(cdr l))) )
		(T
			(sumList (cdr l)))

		))

(defun mainR16(l i)
	(cond
		((null l) 0)
		((atom l) 0)
		( (= (mod (sumList l) 2) 0)
			(princ l)
			(princ #\linefeed)
			(+ 1 (apply '+ (mapcar (lambda (x) (mainR16 x (+ i 1))) l))))
		(T
			(apply '+ (mapcar (lambda (x) (mainR16 x (+ i 1))) l)))

		))

(defun successor1(l)
	(cond
		((null l) nil)
		((listp (car l))
			(cons (successor1 (car l)) (successor1(cdr l))) )
		((AND (numberp (car l)) (=(mod (car l) 2) 0))
			(cons (+ 1 (car l)) (successor1(cdr l))) )
		(T
			(cons (car l) (successor1(cdr l))))
		))

(defun succesor2(l)
	(cond
		((null l) nil)
		((AND (numberp l) (=(mod l 2) 0))
			(+ 1 l))
		((atom l)
			l)
		(T
			(mapcar 'succesor2 l))

		))

(defun removeElement(l e)
	(cond
		((null l) nil)
		((AND (atom l) (char= l e) ) nil)
		((atom l) l)
		(T
			(mapcar (lambda (x) (removeElement x e)) l))

		))

(defun removeElem(l e)
	(cond
		((null l) nil)
		((AND (characterp (car l)) (NOT(char= (car l) e)))
			(cons (car l) (removeElem (cdr l) e)))
		((atom (car l)) 
			(cons (car l) (removeElem (cdr l) e)))
		((listp (car l))
			(cons (removeElem (car l) e) (removeElem (cdr l) e)))
		(T
			(removeElem(cdr l) e))

		))


(defun twiceNthElement(l n i)
	(cond
		((null l) nil)
		((= (mod i n) 0)
			(append (list(car l)) (list(car l)) (twiceNthElement (cdr l) n (+ i 1))))
		(T
			(append (list(car l)) (twiceNthElement (cdr l) n (+ i 1))))
		))

(defun F(l)
	(cond
		((null l) 0)
		((> (F(car l)) 2)
			(+ (car l) (F (cdr l))))
		(T
			(F(car l))) 
		))

(defun caleArbore(l e c)
	(cond
		((AND (numberp l) (= e l))
			c)
		((listp l)
			(mapcan (lambda (x) (caleArbore x e (append  c (list (car l))))) l))
		))

(defun removeNil(l)
	(cond
		((numberp (car l)) )
		))

(defun isMember(l e)
	(cond
		((AND (atom l) (= e l))
			1)
		((atom l)
			0)
		((listp l)
			(apply '+(mapcar (lambda (x) (isMember x e) ) l)))
		))

(defun isMemberWrapper(l e)
	(cond
		((= (isMember l e) 0) 
			nil)
		(T
			T)
		))


(defun increase(x)
	(+ 1 x))
(defun testt()
	(setf a 1)
	(increase a)
	(princ "R:")
	(princ a)
	(princ "end"))

(defun replaceE(l e1 e2 e3 re)
	(cond
		((null l) nil)
		((AND (AND (equal (car l) e1) (equal (car (cdr l)) e2) ) (equal (car(cdr(cdr l))) e3) )
			(append (list re) (replaceE (cdr (cdr (cdr l))) e1 e2 e3 re )))
		(T
			(append (list(car l)) (replaceE (cdr l) e1 e2 e3 re)))


	)
)

(defun calculate(s e1 e2)
	(cond
		((equal s "*")
			(* e1 e2))
		((equal s "/")
			(/ e1 e2))
		((equal s "+")
			(+ e1 e2))
		((equal s "-")
			(- e1 e2))
	)
)

(defun isOperation(s)
	(cond
		((equal s "*")
			T)
		((equal s "/")
			T)
		((equal s "+")
			T)
		((equal s "-")
			T)
		(T
			nil)

	)
)

(defun len(l)
	(cond
		((null l) 0)
		(T
			(+ 1 (length (cdr l))))
	)
)

(defun calcMain(l)
	(cond
		((null l) nil)
		((AND (AND (isOperation(car l)) (numberp (car(cdr l)))) (numberp (car (cdr (cdr l)))))
			(append (list (calculate (car l) (car (cdr l)) (car (cdr (cdr l))))) (calcMain (cdr (cdr (cdr l))) )))
		(T
			(append (list (car l)) (calcMain (cdr l))) )
	)
)


(defun calcMainWrapper(l)
	(cond
		((= (len l) 1) l)
		(T
			(calcMainWrapper (calcMain l)))
	)
)