; definiti o functie care elimina toate aparitiile unui element dintr-o lista neliniara.

;elimin(l1, ..., ln, e) = 
; 						0/, l = 0/ (0/ - lista vida)
;						l1 + elimin(l1, ..., ln, e), l1 != e si l1 - atom
;						elimin(l2, ..., ln, e), l1 = e
;						elimin(l1 + elimin(l2, ...n ln, e)), alftel

;punctul a
(defun inmultire_vectori(l1 l2)
	(cond
		((null l1) l2)
		((null l2) l1)

		((cons (* (car l1) (car l2)) (inmultire_vectori (cdr l1) (cdr l2)) ))
		))

;punctul b

(defun adancime(l v)
	(cond
		((null l) (cons v nil))
		((atom (car l))
			(adancime (cdr l) v))
		(
			(append (adancime (car l) (+ v 1))(adancime (cdr l) v)) 
			)
		))

(defun adancimeWrapper(l)
	(cond
		((maxim (adancime l 1) 0))
		))

;punctul c
(defun elimin(l e)
	(cond
		((null l) nil)
		((= (car l) e)
			(elimin (cdr l) e))
		((atom (car l))
			(cons (car l)(elimin(cdr l) e)))

		(T (cons (elimin(car l) e)
			(elimin (cdr l) e)))
		)


	)

(defun minim(l m)
	(cond
		((null l) m)
		((< (car l) m)
			(minim(cdr l) (car l)))
		((minim (cdr l) m))

		)
	)

(defun sortare(l)
	(cond
		((null l) nil)
		((cons (minim l 10000) (sortare (elimin l (minim l 10000)))))
		)
	)


;punctul d
(defun maxim(l m)
	(cond
		((null l) m)
		((> (car l) m)
			(maxim(cdr l) (car l)))
		((maxim (cdr l) m))

		)
	)

(defun is_in_list(l e)
	(cond
		((null l) 0)
		((= (car l) e)
			1)
		((is_in_list(cdr l) e))
		))

(defun intersectie(l1 l2)
	(cond
		((null l1) nil)
		((null l2) nil)
		((= (is_in_list l2 (car l1)) 1)
			(cons (car l1) (intersectie (cdr l1) l2)))
		((intersectie (cdr l1) l2))
		))




;not used


(defun lungime(l v)
	(cond
		((null l) v)
		((lungime (cdr l) (+ v 1)))

		))

