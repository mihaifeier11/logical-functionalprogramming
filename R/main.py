import copy

class Nod:
    def __init__(self, e):
        self.e = e
        self.urm = None

class Lista:
    def __init__(self):
        self.prim = None


'''
crearea unei liste din valori citite pana la 0
'''
def creareLista():
    lista = Lista()
    lista.prim = creareLista_rec()
    return lista

def creareListaVida():
    lista = Lista()
    return lista

def creareLista_rec():
    x = int(input("x="))
    if x == 0:
        return None
    else:
        nod = Nod(x)
        nod.urm = creareLista_rec()
        return nod

def eListaVida(lista):
    if lista.prim == None:
        return True
    else:
        return False

def adaugaInceput(elem, lista):
    aux = Nod(elem)
    aux.urm = lista.prim
    lista.prim = aux

def primElement(lista):
    return lista.prim.e

def sublista(lista):
    lista.prim = lista.prim.urm
    return lista

def unElement(lista):
    if lista.prim.urm == None:
        return True
    return False

def adaugaSfarsit(it, elem):
    if it.urm == None:
        nod = Nod(elem)
        it.urm = nod
    else:
        adaugaSfarsit(it.urm, elem)

def adaugaSfarsitWrapper(lista, elem):
    if eListaVida(lista):
        adaugaInceput(elem, lista)
    else:
        adaugaSfarsit(lista.prim, elem)

def concatenareListe(lista1, lista2, listaRez):
    if eListaVida(lista1) and eListaVida(lista2):
        return listaRez

    if not eListaVida(lista1):
        adaugaSfarsitWrapper(listaRez, primElement(lista1))
        return concatenareListe(sublista(lista1), lista2, listaRez)

    adaugaSfarsitWrapper(listaRez, primElement(lista2))
    return concatenareListe(lista1, sublista(lista2), listaRez)

'''
tiparirea elementelor unei liste
'''
def tipar(lista):
    tipar_rec(lista.prim)

def tipar_rec(nod):
    if nod != None:
        print (nod.e)
        tipar_rec(nod.urm)




'''
program pentru test
'''

def eInLista(elem, lista):
    if eListaVida(lista):
        return False
    if elem == primElement(lista):
        return True

    return eInLista(elem, sublista(lista))

def diferenta(lista1, lista2, listaRez):
    if eListaVida(lista1):
        return listaRez

    if not eInLista(primElement(lista1), copy.deepcopy(lista2)):
        adaugaInceput(primElement(lista1), listaRez)
        return diferenta(sublista(lista1), lista2, listaRez)

    return diferenta(sublista(lista1), lista2, listaRez)

def diferentaWrapper(lista1, lista2):
    return diferenta(lista1, lista2, creareListaVida())

def insumare_wrapper(lista):
    return insumare(lista, 1)

def insumare(lista, i):
    if eListaVida(lista):
        return 0

    if i % 2 == 0:
        return primElement(lista) + insumare(sublista(lista), i + 1)

    return -primElement(lista) + insumare(sublista(lista), i + 1)



def main():
    list1 = creareLista()
    list2 = creareLista()

    tipar(concatenareListe(list1, list2, creareListaVida()))



    # tipar(sublista(list))
    # print (eListaVida(list))
    # print (primElement(list))
    # tipar(list)

main()


